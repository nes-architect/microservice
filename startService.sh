#!/bin/bash

cd services/$1
npm i
npm run build
npm run start

exit 0;