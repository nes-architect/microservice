import mongoose from "mongoose";

/**
 * Define the properties of the item
 */
interface objInterface {
    createdAt?: string,                         // A date in human readable format
    user_id: mongoose.Schema.Types.ObjectId,    // the id of the owner of the topic
    topic_id: mongoose.Schema.Types.ObjectId,   // the id of the parent topic    
    message: string                             // the message
}

const schema : mongoose.Schema = new mongoose.Schema<objInterface>({
    createdAt: {
        type: String, 
        default: (new Date()).toLocaleDateString()
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    topic_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    message: {
        type: String,
        required: true
    }
})

let Model = mongoose.model<objInterface>('Post', schema);
export default class Topic extends Model {

} 