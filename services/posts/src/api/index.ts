import Express from "express";

import posts from "./routes/posts";

/**
 * Contains only one static function to bind all routes to the Express APP pass in parameters
 */
export default class Api {

    public static bind(app : Express.Application){
        app.use("/api/posts", posts);   // All routes for posts methods
    }

}