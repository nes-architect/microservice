import Express from "express";
import Post from "../../database/models/posts";

import {isConnected} from "../../middlewares/authrequired";

// Route endpoint is stored with a Express.Router() object, so we need it to pass it to a .use() function of Express 
let router = Express.Router();

/**
 * Return all the items of this ressource
 */
router.get("/", isConnected, async (req,res) => {
    // Find all item from the collection and retrieve only properties of its with 'lean' method
    res.json( await Post.find( (req.query != {}) ? req.query : {} ).lean() );
});

/**
 * Create a new item in this ressource
 */
router.post("/", isConnected, async (req,res) => {
    
    try{
        let item : Post = new Post(req.body);
        item.user_id = res.locals.id;
        await item.save();
        res.status(201).json(item.toJSON());
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }
})

/**
 * Get an item in this ressource from his ID
 * check if we have filter
 */
router.get("/:ID", isConnected, async (req,res) => {
    let item : Post|null = await Post.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");
    
    res.json( item.toJSON() );
});

/**
 * Update an item in this ressource from his ID
 */
router.put("/:ID", isConnected, async (req,res) => {
    let item : Post|null = await Post.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");

    try{
        await Post.findOneAndUpdate({_id: item._id}, req.body);
        res.status(201).json( item.toJSON() );
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

/**
 * Delete an item in this ressource from his ID
 */
router.delete("/:ID", isConnected, async (req,res) => {
    let item : Post|null = await Post.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");

    try{
        
        // Delete all posts before topic
        //await Post.deleteMany({topic_id: item._id});
    
        await Post.findOneAndDelete({_id : item._id });
        res.status(200).json({message: "deleted"});
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

/**
 * Delete all item if match with filter pass in query
 */
 router.delete("/", isConnected, async (req,res) => {
    
    if(req.query === {}) return res.status(500).json({message: "You must pass at least one filter in query"});
    
    let items : Post[]|null = await Post.find( req.query );
    if( !items ) return res.status(404).send("Item didn't exists.");

    try{
        
        // Delete all posts before topic
        await Post.deleteMany( req.query );
        res.status(200).json({message: "deleted"});
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

export default router;