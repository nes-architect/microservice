import mongoose from "mongoose";

/**
 * Define the properties of the item
 */
interface objInterface {
    createdAt?: string,                         // A date in human readable format
    user_id: mongoose.Schema.Types.ObjectId,    // the id of the owner of the topic
    title: string                               // The title of the topic
}

const schema : mongoose.Schema = new mongoose.Schema<objInterface>({
    createdAt: {
        type: String, 
        default: (new Date()).toLocaleDateString()
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    title: {
        type: String,
        required: true
    }
})

let Model = mongoose.model<objInterface>('Topic', schema);
export default class Topic extends Model {

} 