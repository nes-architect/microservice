import mongoose from "mongoose";    // Package to connect and manager database mongodb
import config from "config";        // Package to manage json config file stored in config/ folder

/**
 * Database class to init the connexion to MongoDB
 */
export default class Database{

    constructor(){}

    static async init(){
        
        console.log("Connecting to mongodb...")
        try{
            let uri = `mongodb+srv://${config.get("user")}:${config.get("pass")}@${config.get("host")}/${config.get("name")}`;
            await mongoose.connect(uri, config.get("options"));
            console.log("Connected to MongoDB !");
        }
        catch(e:any){
            console.log("Error happen => " + e );
            throw e;
        }

    }

}