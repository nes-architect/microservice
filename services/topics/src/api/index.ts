import Express from "express";

import topics from "./routes/topics";

/**
 * Contains only one static function to bind all routes to the Express APP pass in parameters
 */
export default class Api {

    public static bind(app : Express.Application){
        app.use("/api/topics", topics);   // All routes for topics methods
    }

}