import Express from "express";
import Topic from "../../database/models/topics";

import {isConnected} from "../../middlewares/authrequired";

// Route endpoint is stored with a Express.Router() object, so we need it to pass it to a .use() function of Express 
let router = Express.Router();

/**
 * Return all the items of this ressource
 */
router.get("/", isConnected, async (req,res) => {
    // Find all item from the collection and retrieve only properties of its with 'lean' method
    res.json( await Topic.find( (req.query != {}) ? req.query : {} ).lean() );
});

/**
 * Create a new item in this ressource
 */
router.post("/", isConnected, async (req,res) => {
    try{
        let item : Topic = new Topic(req.body);
        item.user_id = res.locals.id;
        await item.save();
        res.status(201).json(item.toJSON());
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }
})

/**
 * Get an item in this ressource from his ID
 */
router.get("/:ID", isConnected, async (req,res) => {
    let item : Topic|null = await Topic.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");
        
    res.json( item.toJSON() );
});

/**
 * Get all posts from an item in this ressource from his item ID
 */
router.get("/:ID/posts", isConnected, async (req,res) => {
    //let items = await Post.find({topic_id: req.params.ID});
    res.status(500).json( {message: "Deprecated and removed"} );
});

/**
 * Update an item in this ressource from his ID
 */
router.put("/:ID", isConnected, async (req,res) => {
    let item : Topic|null = await Topic.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");

    try{
        await Topic.findOneAndUpdate({_id: item._id}, req.body);
        res.status(201).json( item.toJSON() );
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

/**
 * Delete an item in this ressource from his ID
 */
router.delete("/:ID", isConnected, async (req,res) => {
    let item : Topic|null = await Topic.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");

    try{
        
        // Delete all posts before topic
        //await Post.deleteMany({topic_id: item._id});
    
        await Topic.findOneAndDelete({_id : item._id });
        res.status(200).json({message: "deleted"});
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

export default router;