// Import classes 
import Database from "./database";
import Server from "./server";

// Async main function
async function main(){

    // Gentle message
    console.log("Starting topic service app...");

    // Load Database MongoDB
    await Database.init();
    
    // Start the server
    let server = new Server();
    server.start();
}

// Run the APP
main();