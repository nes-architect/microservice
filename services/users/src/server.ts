import Express from "express";
import morgan from "morgan";
import cookieParser from "cookie-parser";
import cors from "cors";
import Api from "./api";

export default class Server {

    public app : Express.Application;

    constructor(){
        this.app = Express();
        this.configure();
    }

    /**
     * Start the server on with the prot defined by PORT variable or 3000 if not found
     * It listen on all IPS if not defined
     * @param listenOn 
     * @param port 
     */
    public start(listenOn : string = "0.0.0.0", port : number = <number><unknown>process.env.PORT || 3000){
        this.app.listen(port, listenOn, () => {
            console.log("Server is running on port ",port);
        });
    }

    /**
     * Configure the expressjs with middlewares
     */
    private configure(){
        this.app.use( morgan('dev') );                          //-> Display http server access logs in the console
        this.app.use( Express.json() );                         //-> Allow JSON parse in req.body
        this.app.use( Express.urlencoded({extended: true}) );   //-> Allow URLEncoded in req.body
        this.app.use( cookieParser() );                         //-> Enable feature for cookie parsing in express
        this.app.use( cors() );                                 //-> Allow cors operation between cross domains (usefull when services not running on same url than the frontend)

        // Bind all routes to the express endpoints
        Api.bind(this.app);

        // Add keep alive route for healthcheck
        this.app.get("/api/keepalive", async (req: Express.Request, res : Express.Response) => {
            res.json({message: "I'm alive"});
        })

    }

}