import mongoose from "mongoose";

/**
 * Define the properties of the item
 */
interface objInterface {
    createdAt?: string, // A date in human readable format
    username: string,   // the username
    password: string    // a weak password in plain text... don't put serious datas on it
}

const schema : mongoose.Schema = new mongoose.Schema<objInterface>({
    createdAt: {
        type: String, 
        default: (new Date()).toLocaleDateString()
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
})

let Model = mongoose.model<objInterface>('User', schema);
export default class User extends Model {

} 