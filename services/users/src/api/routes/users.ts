import Express from "express";
import User from "../../database/models/users";

import {isConnected} from "../../middlewares/authrequired";

// Route endpoint is stored with a Express.Router() object, so we need it to pass it to a .use() function of Express 
let router = Express.Router();

/**
 * Return all the items of this ressource
 */
router.get("/", isConnected, async (req,res) => {
    // Find all item from the collection and retrieve only properties of its with 'lean' method
    res.json( await User.find( (req.query != {}) ? req.query : {} ).lean() );
});

/**
 * Return all info of the authenticated user, helped from the decoded token gave by idConnected function
 */
router.get("/me", isConnected, async (req,res) => {
    res.json( res.locals );
});

/**
 * Create a new item in this ressource
 */
router.post("/", isConnected, async (req,res) => {
    try{
        let item : User = new User(req.body);
        await item.save();
        res.status(201).json(item.toJSON());
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }
})

/**
 * Get an item in this ressource from his ID
 */
router.get("/:ID", isConnected, async (req,res) => {
    let item : User|null = await User.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");
        
    res.json( item.toJSON() );
});

/**
 * Update an item in this ressource from his ID
 */
router.put("/:ID", isConnected, async (req,res) => {
    let item : User|null = await User.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");

    try{
        await User.findOneAndUpdate({_id: item._id}, req.body);
        res.status(201).json( item.toJSON() );
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

/**
 * Delete an item in this ressource from his ID
 */
router.delete("/:ID", isConnected, async (req,res) => {
    let item : User|null = await User.findOne({_id: req.params.ID});
    if( !item ) return res.status(404).send("Item didn't exists.");

    try{
        await User.findOneAndDelete({_id : item._id });
        res.status(200).json({message: "deleted"});
    }
    catch(e:any){
        res.status(500).json({message: e || e.message });
    }

});

export default router;