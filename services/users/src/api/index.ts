import Express from "express";

import auth from "./routes/auth";
import users from "./routes/users";

/**
 * Contains only one static function to bind all routes to the Express APP pass in parameters
 */
export default class Api {

    public static bind(app : Express.Application){
        app.use("/api/auth", auth);     // All routes for auth methods
        app.use("/api/users", users);   // All routes for users methods
    }

}